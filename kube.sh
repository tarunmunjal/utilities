#!/bin/bash

function pod_log()
{
    allpods=( $(kubectl get pods -n $NAMESPACE -o=jsonpath="{.items[*].metadata.name}") )
    for pod in ${allpods[@]};
    do
        if [[ "$pod" =~ "${1}" ]]; then
            if ! [[ -z "$2" ]]; then
                kubectl -n $NAMESPACE logs $pod -c $2
            else
                kubectl -n $NAMESPACE logs $pod
            fi
        fi
    done
}
function exec_into_pod()
{
    allpods=( $(kubectl get pods -n $NAMESPACE -o=jsonpath="{.items[*].metadata.name}") )
    for pod in ${allpods[@]};
    do
        if [[ "$pod" =~ "${1}" ]]; then
            if ! [[ -z "$2" ]]; then
                kubectl -n $NAMESPACE exec -it $pod -c $2 -- /bin/bash
            else
                kubectl -n $NAMESPACE exec -it $pod -- /bin/bash
            fi
        fi
    done
}
function describe_pod()
{
    allpods=( $(kubectl get pods -n $NAMESPACE -o=jsonpath="{.items[*].metadata.name}") )
    for pod in ${allpods[@]};
    do
        if [[ "$pod" =~ "${1}" ]]; then
            kubectl -n $NAMESPACE describe pod $pod
        fi
    done
}

function delete_pod()
{
    allpods=( $(kubectl get pods -n $NAMESPACE -o=jsonpath="{.items[*].metadata.name}") )
    for pod in ${allpods[@]};
    do
        if [[ "$pod" =~ "${1}" ]]; then
            echo "deleting pod $pod"
            kubectl -n $NAMESPACE delete pod $pod
        fi
    done
}

function delete_deploy()
{
    alldeployments=($(kubectl get deployment -n $NAMESPACE -o=jsonpath="{.items[*].metadata.name}") )
    for deployment in ${alldeployments[@]};
    do
        if [[ "$deployment" =~ "${1}" ]]; then
            echo "deleting deployment $deployment"
            kubectl -n $NAMESPACE delete deployment $deployment
        fi
    done
}

function export_pod_logs() {
    allpods=( $(kubectl get pods -n $NAMESPACE -o=jsonpath="{.items[*].metadata.name}") )
    for pod in ${allpods[@]};
    do
        allcontainers=( $(kubectl get pods -n $NAMESPACE -o jsonpath="{.items[*].spec.containers[*].name}") )
        for container in ${allcontainers[@]};
        do
            echo $container.log
            kubectl logs $pod -c $container -n $NAMESPACE > $container.log
        done
    done
}


function script_usage_help()
{
    echo "Script usage: "
    echo "kube cluserinfo"
    echo "kube describe <partial pod name>"
    echo "kube delete <partial deployment name>"
    echo "kube delete <partial_pod_name>"
    echo "kube exec <partial_pod_name> optional <container_name> only supports /bin/bash"
    echo "kube logs <partial_pod_name>"
    exit 0
}

function check_namespace_env_variable()
{
    if [[ -z "$NAMESPACE" ]]; then
        echo "Please make sure an environment variable `NAMESPACE` is set to the namespace you want to target"
        exit 2
    else
        echo "Targeting Namespace: $NAMESPACE"
    fi
}



if ! [[ -z "$1" ]]; then
    if [[ "$1" == "help" ]]; then
        script_usage_help
    fi
    check_namespace_env_variable
    if [[ "$1" == "clusterinfo" ]]; then
        kubectl config get-contexts
    elif [[ "$1" =~ "log" ]]; then
        if ! [[ -z "$2" ]]; then
            pod_log "${@:2}"
        fi
    elif [[ "$1" =~ "exec" ]]; then
        if ! [[ -z "$2" ]]; then
            exec_into_pod "${@:2}"
        fi
    elif [[ "$1" =~ "describe" ]]; then
        if ! [[ -z "$2" ]]; then
            describe_pod $2
        fi
    elif [[ "$1" =~ "deletepod" ]] || ( [[ "$1" == "delete" ]] && [[ "$2" =~ "pod" ]] ) ; then
        if [[ "$1" == "delete" ]]; then
            delete_pod $3
        else
            delete_pod $2
        fi
    elif [[ "$1" =~ "deletedeploy" ]] || ( [[ "$1" == "delete" ]] && [[ "$2" =~ "deploy" ]] ) ; then
        if [[ "$1" == "delete" ]]; then
            delete_deploy $3
        else
            delete_deploy $2
        fi
    elif [[ "$1" =~ "getpod" ]] || ( [[ "$1" =~ "get" ]] && [[ "$2" =~ "pod" ]] ) ; then
        kubectl get pods -n $NAMESPACE
    elif [[ "$1" =~ "getserv" ]] || ( [[ "$1" =~ "get" ]] && [[ "$2" =~ "servi" ]] ) ; then
        kubectl get service -n $NAMESPACE
    elif [[ "$1" =~ "exportlogs" ]] || ( [[ "$1" =~ "export" ]] && [[ "$2" =~ "logs" ]] ) ; then
        export_pod_logs
    else
        kubectl -n $NAMESPACE "${@}"
    fi
else
    script_usage_help
fi
