## python class of groovy scripts for jenkins

class JenkinsGroovyScripts():

    def __init__(self):
        pass

    @property
    def list_plugins(self):
        return r"""
		def plugin_list = []
		Jenkins.instance.pluginManager.plugins.each{ plugin ->
            //println ("${plugin.getDisplayName()} (${plugin.getShortName()}): ${plugin.getVersion()}")
            plugin_list.add("${plugin.getShortName()}:${plugin.getVersion()}")
        }
		plugin_list.sort().each{
            println(it)
        }
        return
        """

    @property
    def list_plugins_without_version(self):
        return r"""
		def plugin_list = []
		Jenkins.instance.pluginManager.plugins.each{ plugin ->
            //println ("${plugin.getDisplayName()} (${plugin.getShortName()}): ${plugin.getVersion()}")
            plugin_list.add("${plugin.getShortName()}")
        }
		plugin_list.sort().each{
            println(it)
        }
        return
        """

    @property
    def get_job_information(self):
        return r"""
        import jenkins.model.*
        // pass in all the ids

        list_m = ["<credentialsId>drca_aws_credentials</credentialsId>"]
        list_m.each{ pattern ->
        Jenkins.instance.items.findAll { item ->
        def match = item.configFile.file.find { it.contains(pattern) } != null
        if (match) {
                //item.configFile.file.findAll { it.contains('AWS_PROFILE') }.each {
                    //println("${(it.trim().replace('<credentialsId>','')).replace('</credentialsId>','')}, ${item.name}")
                //}
                println("${item.name}")
            }
            }
        }
        return

        """

    @property
    def delete_all_job(self):
        return r"""
        import jenkins.model.*

        def matchedJobs = Jenkins.instance.items.each { job ->
            exception_list = ["test1"]
            if (exception_list.contains(job.name)) {
                println ("Job ${job.name} is in exception list and will not be deleted.")
            }
            else {
                println("Deleting Job " + job.name)
                job.delete()
            }
        }

        return
        """

    @property
    def list_all_jobs(self):
        return r"""
        import jenkins.model.*

        def matchedJobs = Jenkins.instance.items.each { job ->
            println("Job Name " + job.name)
        }
        return
        """

    @property
    def disable_timed_jobs(self):
        return r"""
        import hudson.model.*
        import hudson.triggers.*

        for(item in Hudson.instance.items) {
        for(trigger in item.triggers.values()) {
            if(trigger instanceof TimerTrigger) {
            println("--- Timer trigger for " + item.name + " ---")
            if(item.disabled) {
                println("Job " + item.name + " is already disabled")
            } else {
                println("Disabling " + item.name)
                println(trigger.spec + '\n')
                item.disabled = true
                item.save()
            }
            }
        }
        }
        return
        """

