#!/usr/bin/env python
import os
from choicemenu import ChoiceMenu
from groovyscripts import JenkinsGroovyScripts

def get_jjb_vars():
    env_dic = {}
    with open(
        os.path.expanduser("~/.config/jenkins_jobs/jenkins_jobs.ini"), "r"
    ) as jjbfile:
        for line in jjbfile.readlines():
            if (
                line.startswith("url")
                or line.startswith("password")
                or line.startswith("user")
            ):
                key_value = line.split("=")
                env_dic[key_value[0]] = key_value[1].strip()
    return env_dic

def execute_groovy_script_jenkins(jenkins_url, username, password, script):
    import requests

    print("executing script on jenkins {}".format(jenkins_url))
    post_response = requests.post(
        url=jenkins_url, auth=(username, password), data={"script": script}
    )
    return post_response


if __name__ == "__main__":
    JGS = JenkinsGroovyScripts()
    script_names = [script_opt for script_opt in dir(JGS) if not script_opt.startswith("__")]
    script_names.sort()
    secondlevel_uc = ChoiceMenu(
        script_names,
        multichoice=False,
    ).get_choice()

    jenkins_vars = get_jjb_vars()
    print(
        execute_groovy_script_jenkins(
            jenkins_vars["url"] + "/" + "scriptText",
            jenkins_vars["user"],
            jenkins_vars["password"],
            script=getattr(JGS, secondlevel_uc[0]),
        ).text
    )

